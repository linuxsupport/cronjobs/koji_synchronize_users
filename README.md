# koji_synchronize_users

This repository holds files and scripts for automating the sychronization of users in koji.

The code is scheduled to run every day within a container in Nomad.

The main Python script koji_synchronize_users.py manages CERN users in koji based on their registration in the e-group 'koji-users'.

Workflow
- The script gets all the members of the e-group 'koji-users' (egroup_users) and all users in koji (koji_users).

- Comparison based on the e-group users (egroup_users):
    - If the user exists in koji but is deactivated (status=1). ACTION: will be activated (status=0).
    - If the user does not exist in koji. ACTION: this user will be added in koji

- Comparison based on the users in koji (koji_users):
    - If a user is active in koji (status=0), but does not exist in the e-group. ACTION: will be deactivated (status=1).

Basic configuration
- THE_UNTOUCHABLES variable defines users that are not registered in the e-group 'koji-users', but must not be deactivated.
- MAX_ACTIONS variable defines the most actions that can happen in one run for any set of actions (activating, deactivating or adding users in koji). Can be changed if needed.
- NO_ACTION variable: 
    - If True, the script runs and prints all actions, but does not do any of them.
    - If False, the script runs normally.

Notes
- For any succesful action and any error the members of the e-group 'koji-admins' will get an email.
