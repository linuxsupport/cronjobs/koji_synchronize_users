job "${PREFIX}_koji_synchronize_users" {
  datacenters = ["*"]
  
  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_koji_synchronize_users" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/koji_synchronize_users/koji_synchronize_users:${CI_COMMIT_SHORT_SHA}"

      logging {
        config {
          tag = "${PREFIX}_koji_synchronize_users"
        }
      }
    }
    env {
      ADMIN_EMAIL = "$ADMIN_EMAIL"
      KOJI_SERVER = "$KOJI_SERVER"
      THE_UNTOUCHABLES = "$THE_UNTOUCHABLES"
      KOJI_ADMIN = "$KOJICI_USER"
      KOJI_ADMIN_PWD = "$KOJICI_PWD"
      MAX_ACTIONS = "$MAX_ACTIONS"
      LDAPHOST = "$LDAPHOST"
      EGROUP_USERS = "$EGROUP_USERS"
      EGROUP_ADMINS = "$EGROUP_ADMINS"
      NO_ACTION = "$NO_ACTION"
      GIT_URL = "$GIT_URL"
      GIT_BRANCH = "$GIT_BRANCH"
      ACL_YAML_PATH = "$ACL_YAML_PATH"
      TAG = "${PREFIX}_koji_synchronize_users"
    }

    resources {
      cpu = 3000 # Mhz
      memory = 4096 # MB
    }

  }
}
