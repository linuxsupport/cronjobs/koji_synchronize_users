#!/bin/bash/python3

import ldap
import koji

import re
import subprocess
import sys
import os
import tempfile
import time
import requests
import yaml
from distutils.util import strtobool

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

THE_UNTOUCHABLES = os.getenv('THE_UNTOUCHABLES')
ADMIN_EMAIL = os.getenv('ADMIN_EMAIL')
KOJI_ADMIN = os.getenv('KOJI_ADMIN')
KOJI_ADMIN_PWD = os.getenv('KOJI_ADMIN_PWD')
EGROUP_USERS = os.getenv('EGROUP_USERS')
EGROUP_ADMINS = os.getenv('EGROUP_ADMINS')
MAX_ACTIONS = int(os.getenv('MAX_ACTIONS'))
NO_ACTION = strtobool(os.getenv('NO_ACTION'))
KOJI_SERVER = os.getenv('KOJI_SERVER')
GIT_URL = os.getenv("GIT_URL")
GIT_BRANCH = os.getenv("GIT_BRANCH")
ACL_YAML_PATH = os.getenv("ACL_YAML_PATH")


"""
	Main method for comparing CERN users of e-group defined in EGROUP_USERS
    against CERN users that are added in koji (either prod or kojitest).
    Errors are send to ADMIN_EMAIL.
"""
def main():

    ## Main variables
    LDAPHOST = os.getenv('LDAPHOST')

    if (THE_UNTOUCHABLES is None or KOJI_SERVER is None or KOJI_ADMIN is None or KOJI_ADMIN_PWD is None or GIT_URL is None or GIT_BRANCH is None or ACL_YAML_PATH is None):
        subject = "Koji synchronize users - Main variables are missing."
        body = f"ERROR: One of the variables THE_UNTOUCHABLES, KOJI_SERVER, KOJI_ADMIN, KOJI_ADMIN_PWD, GIT_URL, GIT_BRANCH or ACL_YAML_PATH are not defined. Exiting."
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(0)
    the_untouchables = set([item.strip() for item in THE_UNTOUCHABLES.split(",")])
    if len(the_untouchables) == 0 or the_untouchables is None:
        subject = "Koji synchronize users - No koji users to be excluded."
        body = f"ERROR: The list 'THE_UNTOUCHABLES' for excluding koji users from deactivation has no users in it."
        body += f"\n\nPlease, check which of the following users scheduled for deactivation are going to be excluded or not:"
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(0)

    ## Connect to LDAP to retieve users from egroup(s)
    try:
        ldap_conn = ldap.initialize(f"ldap://{LDAPHOST}")
    except ldap.LDAPError:
        subject = "Koji synchronize users - LDAP error"
        body = f"ERROR: Unable to connect to LDAP '{LDAPHOST}' and get users of '{EGROUP_USERS}' e-group."
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(1)
    egroup_users = cern_egroup_get_users(EGROUP_USERS, ldap_conn, [], True)
    egroup_admins = cern_egroup_get_users(EGROUP_ADMINS, ldap_conn, [], True)
    print(f"{len(egroup_users)} are the total EGROUP_USERS: '{EGROUP_USERS}' e-group.")
    print(f"{len(egroup_admins)} are the total EGROUP_ADMINS: '{EGROUP_ADMINS}' e-group.")

    ## Connect to koji to retieve users registered as builders.
    try:
        koji_client_session = koji.ClientSession(KOJI_SERVER)
    except koji.GenericError:
        subject = "Koji synchronize users - Koji error"
        body = f"ERROR: Unable to connect to koji server: {KOJI_SERVER}"
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(1)
    try:
        koji_users = koji_client_session.listUsers(userType=0)
    except requests.exceptions.HTTPError:
        subject = "Koji synchronize users - Koji error"
        body = f"ERROR: Unable to get a list of koji users, while connecting on koji server: {KOJI_SERVER}"
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(1)
    print(f"{len(koji_users)} are the total koji users in: {KOJI_SERVER}")

    # Simple check on size of users. Neither e-group nor koji users are supposed to be 0.
    no_user_retrieved = False
    body = ""
    if len(egroup_users) == 0:
        no_user_retrieved = True
        body += f"ERROR: The number of users in '{EGROUP_USERS}' e-group is {len(egroup_users)}. You should check whether something is wrong about getting e-group information."
    elif len(egroup_admins) == 0:
        no_user_retrieved = True
        body = f"ERROR: The number of users in '{EGROUP_ADMINS}' e-group is {len(egroup_users)}. You should check whether something is wrong about getting e-group information."
    elif len(koji_users) == 0:
        no_user_retrieved = True
        body += f"ERROR: The number of koji users is {len(koji_users)}. You should check whether something is wrong about getting koji users."
    if no_user_retrieved:
        subject = "Koji synchronize users - Possible issue on retrieving users"
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(0)

    ## Compare CERN users registered in e-group 'koji-users' VS CERN users added in koji.
    must_not_admin_perm = egroup_users.difference(egroup_admins)    # users registered ONLY in EGROUP_USERS (not in EGROUP_ADMINS), must NOT have admin permission.
    egroup_reg_accounts = egroup_users.union(egroup_admins)         # taking into consideration all CERN accounts that are registered both in EGROUP_USERS and EGROUP_ADMINS
    eusers_to_activate, eusers_not_koji, koji_users_to_deactivate = users_compare(egroup_reg_accounts, koji_users, the_untouchables)
    koji_users_to_admin, eadmins_not_koji, koji_users_revoke_admin = admins_compare(egroup_admins, must_not_admin_perm, koji_client_session)
    eusers_koji_add = eusers_not_koji.difference(eadmins_not_koji)  # In case a new user is added "simultaneously" in both e-groups (EGROUP_USERS and EGROUP_ADMINS).
    # Custom build permissions
    custom_build_add, custom_build_remove = query_custom_build_permissions(koji_client_session, ldap_conn, egroup_users)


    if NO_ACTION:
        koji_client_session.logout()
        print("\n\nNO ACTION ALLOWED.")
        print("\nUsers to activate are:")
        no_action_report(eusers_to_activate)
        print("\nUsers to deactivate are:")
        no_action_report(koji_users_to_deactivate)
        print("\nUsers to add to koji are:")
        no_action_report(eusers_koji_add)
        print("\nAdmins to give admin perms:")
        no_action_report(koji_users_to_admin)
        print("\nAdmins to revoke admin perms:")
        no_action_report(koji_users_revoke_admin)
        print("\nAdmins to add to koji are:")
        no_action_report(eadmins_not_koji)
        print("\nCustom build permissions to add to koji are:")
        no_action_report(custom_build_add)
        print("\nCustom build permissions to remove from koji are:")
        no_action_report(custom_build_remove)
        sys.exit(0)

    manage_koji_actions(koji_users_to_admin, koji_users_revoke_admin, eadmins_not_koji, "admins", {}, {}, koji_client_session)
    manage_koji_actions(eusers_to_activate, koji_users_to_deactivate, eusers_koji_add, "users", custom_build_add, custom_build_remove, koji_client_session)

    # At this point, we no longer need a LDAP connection
    ldap_conn.unbind()

def query_custom_build_permissions(koji_client_session, ldap_conn, egroup_users):
    """ Function dedicated to querying the custom build- permissions in koji """
    # Get koji::hub::acls content
    clonepath = tempfile.mkdtemp(prefix="lsb-clone")
    do_execute(f"git clone {GIT_URL} repo", clonepath)
    do_execute(f"git checkout {GIT_BRANCH}", f"{clonepath}/repo")

    custom_build_add = {}
    custom_build_remove = {}

    try:
        with open(f"{clonepath}/repo/{ACL_YAML_PATH}", "r", encoding="utf-8") as yaml_file:
            y = yaml.safe_load(yaml_file)
            acls = y["koji::hub::acls"]
            if len(acls) == 0:
                raise IndexError
    except (FileNotFoundError, KeyError, yaml.YAMLError):
        send_email(ADMIN_EMAIL, 'Koji synchronize users - Unable to read/parse ACL yaml path', f"Dear admins,\nToday it was not possible to read/parse the {ACL_YAML_PATH} file.\nProbably a human should check something.\n\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)")
        sys.exit(0)
    except IndexError:
        send_email(ADMIN_EMAIL, 'Koji synchronize users - ACL yaml content is empty', f"Dear admins,\nToday it seems that there were zero ACL entries in {ACL_YAML_PATH}, which doesn't really feel correct.\nProbably a human should check something.\n\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)")
        sys.exit(0)
    # Drop the '*' entry from the ACLs if it exists, it's not an e-group so we can ignore it
    acls.pop('*', None)
    current_build_koji_perms = set()
    # This loop is to generate a set for future use
    acl_permissions = set([f"build-{acl}" for acl in acls])
    for perm in koji_client_session.getAllPerms():
        if perm['name'].startswith('build-'):
            current_build_koji_perms.add(perm['name'])
    koji_build_permissions_to_remove = current_build_koji_perms.difference(acl_permissions)
    for user in egroup_users:
        current_koji_perms = get_koji_perms(koji_client_session, user)
        # We don't need 'build' permissions anymore, remove if present
        if 'build' in current_koji_perms:
            custom_build_remove[user] = [ 'build' ]
        for acl in acls:
            acl_users = cern_egroup_get_users(acl, ldap_conn, [], True)
            # Remove koji perms that shouldn't exist
            for perm in current_koji_perms:
                if perm in koji_build_permissions_to_remove:
                    if user not in custom_build_remove:
                        custom_build_remove[user] = [ perm ]
                    else:
                        if perm not in custom_build_remove[user]:
                            custom_build_remove[user].append(perm)
            if user in acl_users:
                # Add koji perms for a user, if they are not already there
                if f"build-{acl}" not in current_koji_perms:
                    if user not in custom_build_add:
                        custom_build_add[user] = [ f"build-{acl}" ]
                    else:
                        if f"build-{acl}" not in custom_build_add[user]:
                            custom_build_add[user].append(f"build-{acl}")
            else:
                if f"build-{acl}" in current_koji_perms:
                    if user not in custom_build_remove:
                        custom_build_remove[user] = [ f"build-{acl}" ]
                    else:
                        if f"build-{acl}" not in custom_build_remove[user]:
                            custom_build_remove[user].append(f"build-{acl}")
    return custom_build_add, custom_build_remove

def get_koji_perms(koji_client_session, user):
    """ Simple function to return the koji permissions for a user """
    try:
        current_koji_perms = koji_client_session.getUserPerms(user)
    except koji.GenericError:
        current_koji_perms = []
    return current_koji_perms

def do_execute(cmd, tmpdir='/tmp'):
    """ Simple class to execute a binary with subprocess"""
    # pylint: disable=consider-using-with
    process = subprocess.Popen(cmd, cwd=tmpdir, stderr=subprocess.PIPE, shell=True, stdout=subprocess.PIPE)
    out, err = process.communicate()
    if process.wait() != 0:
        print(f"Failed to execute: {cmd}")
        sys.exit(process.wait())


def login_as_admin(session):
    """ Login with admin credentials, return client session object """
    # Log in with a koji user that has admin rights.
    try:
        koji_admin = session.getUser(userInfo=KOJI_ADMIN, strict=True)
    except koji.GenericError:
        subject = "Koji synchronize users - Koji admin not found"
        body = f"ERROR: Koji user '{KOJI_ADMIN}' could not be retrieved from koji database."
        body += "\n\nTherefore, getting the user's 'krb_principals' and connecting as admin in koji in order to execute the scheduled actions is impossible."
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(0)
    # If we are already logged in, return early
    if session.getLoggedInUser() is not None:
        return session
    try:
        session.gssapi_login(
            koji_admin["krb_principals"][0], KOJI_ADMIN_PWD
        )
    except koji.GSSAPIAuthError:
        subject = "Koji synchronize users - Koji admin could not log in koji"
        body = f"ERROR: Koji user '{KOJI_ADMIN}' attempt to connect to koji returned 'GSSAPIAuthError'."
        body += "\n\nCheck the user's koji credentials and Kerberos authorization."
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(0)
    return session


"""
    manage_koji_actions() manages any action that needs to be done in koji:
    activating/deactivating users, granting/revoking admin permissions, creating a new koji user with build/admin rights
    koji_group = 'admins' or 'users'
"""
def manage_koji_actions(koji_accounts_upgrade, koji_accounts_downgrade, koji_accounts_create, koji_group, custom_build_add, custom_build_remove, koji_client_session):

    if len(koji_accounts_upgrade) + len(koji_accounts_downgrade) + len(koji_accounts_create) + len(custom_build_add) + len(custom_build_remove) > 0:
        print(f"\nAt least one action in koji {koji_group} is needed. Koji-admin to log-in:\t{KOJI_ADMIN}")

        # Log in with a koji user that has admin rights.
        koji_client_session = login_as_admin(koji_client_session)

        if koji_group == "users":
            upgraded_accounts, downgraded_accounts, koji_added_accounts, added_build_permissions, removed_build_permissions = actions_koji_users(koji_accounts_upgrade, koji_accounts_downgrade, koji_accounts_create, custom_build_add, custom_build_remove, koji_client_session)
        elif koji_group == "admins":
            upgraded_accounts, downgraded_accounts, koji_added_accounts = actions_koji_admins(koji_accounts_upgrade, koji_accounts_downgrade, koji_accounts_create, koji_client_session)
        koji_client_session.logout()

        if len(upgraded_accounts) + len(downgraded_accounts) + len(koji_added_accounts) + len(custom_build_add) + len(custom_build_remove) > 0:
            if koji_group == "users": success_email_users(upgraded_accounts, downgraded_accounts, koji_added_accounts, added_build_permissions, removed_build_permissions)
            elif koji_group == "admins": success_email_admins(upgraded_accounts, downgraded_accounts, koji_added_accounts)
        else:
            print(f"No succesful actions took place on koji {koji_group} due to an early error. Please, check your email.")

    else:
        print(f"Membership from e-group 'koji-{koji_group}' is already synchronized.")


"""
    no_action_report() prints list of actions.
"""
def no_action_report(list_to_report):
    if len(list_to_report) > 30:
        print(f"Too many to print: {len(list_to_report)}")
    else:
        if isinstance(list_to_report, dict):
            for item in list_to_report.items():
                print(item)
        else:
            for item in list_to_report:
                print(item)

"""
    Arguments:
    e-group -- The e-group of interest.
    conn -- An initialized LDAP connection object
    processed -- A list of already processed e-groups. Pass [].
    recursion -- Boolean to enable recursion
    fmt -- Format string to print usernames. Must have '%s'

    Returns:
    A set of formatted usernames (empty if all e-groups are empty)
"""
def cern_egroup_get_users(egroup, conn, processed, recursion=False, fmt="%s"):

    base = 'OU=e-groups,OU=Workgroups,DC=cern,DC=ch'
    user_regexp = r'CN=(\S+),OU=Users,OU=Organic Units,DC=cern,DC=ch'
    egroup_regexp = r'CN=(\S+),OU=e-groups,OU=Workgroups,DC=cern,DC=ch'

    users = set()
    egroup_filter = "(&(objectClass=group)(CN=%s))" % egroup
    try:
        ldap_results = conn.search_s(base, ldap.SCOPE_SUBTREE, egroup_filter, ['member'])
    except ldap.SERVER_DOWN:
        subject = "Koji synchronize users - LDAP server down"
        body = f"ERROR: Unable to connect to LDAP and get users of '{egroup}' e-group."
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(1)
    for dn, entry in ldap_results:
        if 'member' in entry:
            for result in entry['member']:
                match = re.match(user_regexp, result.decode('utf-8'))
                if match is not None:
                    users.add(fmt % match.group(1))
                    continue
                if recursion == True:
                    match = re.match(egroup_regexp, result.decode('utf-8'))
                    if match is not None:
                        expanded_egroup = cern_egroup_get_users(match.group(1), conn, processed, recursion, fmt)
                        users = users.union(expanded_egroup)
    return users


"""
    users_compare() compares CERN users of a set of accounts registered in e-group(s) against CERN users that are active koji users.
    The resulting groups and the corresponding actions are:
    users that are registered in predefined e-groups, exist in koji, but are deactivated -> to be activated
    users that are registered in predefined e-groups, but do NOT exist in koji -> to be added in koji
    users that exist in koji and are active, but NOT registered in predefined e-groups -> to be deactivated (if not in the_untouchables)
    ARG(s): a set of e-group users (EGROUP_USERS + EGROUP_ADMINS), a list of dictionaries of koji users (1 dict -> 1 user), a set of untouchable users
    RETURN: a set of koji users to be activated, a set of koji users to be deactivated, a set of e-group users to be added in koji
"""
def users_compare(egroup_reg_accounts, koji_users, the_untouchables):

    ## Setting up sets of users
    eusers_koji_active = set()
    eusers_to_activate = set()
    eusers_not_koji = set()

    koji_users_set = set()
    koji_users_active = set()
    koji_users_deactivated = set()
    for koji_user in koji_users:
        koji_users_set.add(koji_user["name"])
        if koji_user["status"] == 0:
            koji_users_active.add(koji_user["name"])
        else:
            koji_users_deactivated.add(koji_user["name"])

    ## Comparison based on egroup_users
    eusers_koji_active = egroup_reg_accounts.intersection(koji_users_active)       # ACTION: Nothing
    eusers_to_activate = egroup_reg_accounts.intersection(koji_users_deactivated)  # ACTION: Activate them
    eusers_not_koji    = egroup_reg_accounts.difference(koji_users_set)            # ACTION: Add users in koji

    # Report on comparison based on egroup_users
    print(f"\nACTIONS BASED ON EGROUP_USERS: '{EGROUP_USERS}' + '{EGROUP_ADMINS}' e-group.")
    print(f"{len(eusers_koji_active)}\tusers in e-groups '{EGROUP_USERS}' + '{EGROUP_ADMINS}' exist in koji and are activated. NO ACTION required.")
    print(f"{len(eusers_to_activate)}\tusers in e-groups '{EGROUP_USERS}' + '{EGROUP_ADMINS}' exist in koji, NOT activated. ACTION: To be activated.")
    print(f"{len(eusers_not_koji)}\tusers in e-groups '{EGROUP_USERS}' + '{EGROUP_ADMINS}' are NOT in koji. ACTION: To be added in koji.")

    ## Comparison based on koji_users
    active_koji_users_in_egroup = set()
    active_koji_users_not_egroup = set()

    active_koji_users_in_egroup  = koji_users_active.intersection(egroup_reg_accounts)  # ACTION: Nothing
    active_koji_users_not_egroup = koji_users_active.difference(egroup_reg_accounts)    # ACTION: Possibly to be deactivated (if not in untouchables)

    # Report on comparison based on koji_users
    print(f"\n{len(active_koji_users_in_egroup)}\tactive users in koji are registered in e-groups '{EGROUP_USERS}' + '{EGROUP_ADMINS}'. NO ACTION required")
    print(f"{len(active_koji_users_not_egroup)}\tactive users in koji are NOT registered in the e-group '{EGROUP_USERS}'. We would deactivated them all, BUT:")
    print(f"\t{len(the_untouchables)} users are in THE_UNTOUCHABLES list.")

    koji_users_to_deactivate = active_koji_users_not_egroup.difference(the_untouchables) # ACTION: Deactivate them
    print(f"\t{len(active_koji_users_not_egroup) - len(koji_users_to_deactivate)} out of {len(active_koji_users_not_egroup)} koji users that are not registered in the e-groups '{EGROUP_USERS}' + '{EGROUP_ADMINS}', are included in the THE_UNTOUCHABLES list. \
            \n{len(koji_users_to_deactivate)}\tkoji users are NOT in the e-group and NOT in THE_UNTOUCHABLES list. ACTION: To be deactivated.")

    ## Checking the number of actions after all comparisons
    is_limit_exceeded = False
    too_many_report = ""
    if len(eusers_to_activate) > MAX_ACTIONS:
        is_limit_exceeded = True
        too_many_report += f"\n{len(eusers_to_activate)} koji users are going to be activated:"
        for euta in eusers_to_activate:
            too_many_report += f"\n{euta}"
    if len(koji_users_to_deactivate) > MAX_ACTIONS:
        is_limit_exceeded = True
        too_many_report += f"\n{len(koji_users_to_deactivate)} koji users are going to be deactivated:"
        for kutd in koji_users_to_deactivate:
            too_many_report += f"\n{kutd}"
    if len(eusers_not_koji) > MAX_ACTIONS:
        is_limit_exceeded = True
        too_many_report += f"\n{len(eusers_not_koji)} koji users are going to be added:"
        for eunk in eusers_not_koji:
            too_many_report += f"\n{eunk}"
    if is_limit_exceeded:
        subject = "Koji synchronize users - Too many actions to take"
        body = f"ERROR: After comparing koji users with e-group users, the number of at least one set of actions for synchronizing users is more than {MAX_ACTIONS}."
        body += f"\n\nPlease, check the following actions:\n"
        body += too_many_report
        body += "\n\nIf you want all of these actions to take place, increase the variable 'MAX_ACTIONS' to the highest number of actions.\nOtherwise, you need to intervene manually."
        send_email(ADMIN_EMAIL, subject, body)
        print(body)
        sys.exit(0)

    return (eusers_to_activate, eusers_not_koji, koji_users_to_deactivate)


"""
    admins_compare() compares CERN users of any e-group defined as EGROUP_ADMINS against CERN users that are active koji users with admin permission.
    The resulting groups and the corresponding actions are:
    Users that are registered in EGROUP_ADMINS, exist in koji, but without admin permission -> to be granted with admin permission
    Users that are registered in EGROUP_ADMINS, but do NOT exist in koji -> to be added in koji with 'admin' permissions.
    Users that exist in koji with admin permission, but are NOT registered in EGROUP_ADMINS -> admin permission will be revoked.
    ARG(s): a set of e-group users (usually EGROUP_ADMINS), a set of users that belong ONLY in EGROUP_USERS, and a koji.ClientSession()
    RETURN: a set of koji users to be granted 'admin' permission, a set of koji users to have 'admin' permission revoked, a set of e-group users to be added in koji with admin permission
"""
def admins_compare(egroup_admins, must_not_admin_perm, koji_client_session):

    ## Setting up sets of users
    koji_admins_verified = set()
    koji_users_to_admin = set()
    admins_not_in_koji = set()

    ## Comparison based on egroup_admins
    for eadmin in egroup_admins:
        try:
            koji_user_perms = koji_client_session.getUserPerms(eadmin)
        except koji.GenericError:
            # If getting koji user permissions returns an error, then this user does not exist in koji.
            admins_not_in_koji.add(eadmin)
            continue
        if "admin" not in koji_user_perms:
            koji_users_to_admin.add(eadmin)
        else:
            koji_admins_verified.add(eadmin)

    ## Checking permissions of users that belong ONLY in EGROUP_USERS
    koji_users_revoke_admin = set()
    for mnap in must_not_admin_perm:
        try:
            koji_user_perms = koji_client_session.getUserPerms(mnap)
        except koji.GenericError:
            # If getting koji user permissions returns an error, then this user does not exist in koji.
            continue
        if "admin" in koji_user_perms:
            koji_users_revoke_admin.add(mnap)

    print(f"\nACTIONS BASED ON EGROUP_ADMINS: '{EGROUP_ADMINS}' e-group.")
    print(f"{len(koji_admins_verified)}\tusers in e-group '{EGROUP_ADMINS}' exist in koji with admin rights. NO ACTION required.")
    print(f"{len(koji_users_to_admin)}\tusers in e-group '{EGROUP_ADMINS}' exist in koji, NO admin rights. ACTION: Grant admin permission.")
    print(f"{len(admins_not_in_koji)}\tusers in e-group '{EGROUP_ADMINS}', but NOT in koji. ACTION: To be added in koji with admin rights.")
    print(f"{len(koji_users_revoke_admin)}\tkoji users exist only in e-group '{EGROUP_USERS}', with admin rights. ACTION: Revoke admin permission.")

    return koji_users_to_admin, admins_not_in_koji, koji_users_revoke_admin


"""
    actions_koji_users() manages all actions that need to take place in koji.
    These actions are: activate a koji user, deactivate a koji user and add a new CERN user in koji.
    ARG:    a set of users to activate, a set of users to deactivate, a set of users to add, a dict of
            custom builds to add/remove and a koji.ClientSession with a logged in user with koji admin rights
    RET:    5 lists of dictionaries: activated koji users, deactivated koji users, newly added koji users,
            newly added custom build permissions and newly removed custom build permissions
"""
def actions_koji_users(eusers_to_activate, koji_users_to_deactivate, eusers_not_koji, custom_build_add, custom_build_remove, koji_admin_session):

    activated_users = []
    deactivated_users = []
    koji_added_users = []
    custom_build_permissions_added = {}
    custom_build_permissions_removed = {}

    # Activating users
    if len(eusers_to_activate) > 0:
        print(f"\nACTION: Going to activate {len(eusers_to_activate)} user(s) in koji.")
        for eua in eusers_to_activate:
            activated_user_status = change_koji_status(eua, 0, koji_admin_session)
            if activated_user_status[0]:
                activated_users.append(activated_user_status[1])
            else:
                subject = "Koji synchronize users - Koji error"
                body = f"ERROR: An error happened while changing status in a koji user.\nError report:\n{activated_user_status[1]}"
                send_email(ADMIN_EMAIL, subject, body)
                print(body)
                return (activated_users, deactivated_users, koji_added_users, custom_build_permissions_added, custom_build_permissions_removed)

    # Deactivating users
    if len(koji_users_to_deactivate) > 0:
        print(f"\nACTION: Going to deactivate {len(koji_users_to_deactivate)} user(s) in koji.")
        for eud in koji_users_to_deactivate:
            deactivated_user_status = change_koji_status(eud, 1, koji_admin_session)
            if deactivated_user_status[0]:
                deactivated_users.append(deactivated_user_status[1])
            else:
                subject = "Koji synchronize users - Koji error"
                body = f"ERROR: An error happened while changing status in a koji user.\n\nError report:\n{deactivated_user_status[1]}"
                send_email(ADMIN_EMAIL, subject, body)
                print(body)
                return (activated_users, deactivated_users, koji_added_users, custom_build_permissions_added, custom_build_permissions_removed)

    # Adding users
    if len(eusers_not_koji) > 0:
        print(f"\nACTION: Going to add {len(eusers_not_koji)} e-group_users in koji.")
        for eu in eusers_not_koji:
            koji_added_user_status = add_to_koji(eu, [], koji_admin_session)
            if koji_added_user_status[0]:
                koji_added_users.append(koji_added_user_status[1])
            else:
                subject = "Koji synchronize users - Koji error"
                body = f"ERROR: An error happened while adding a new user in koji.\nError report:\n{koji_added_user_status[1]}"
                send_email(ADMIN_EMAIL, subject, body)
                print(body)
                return (activated_users, deactivated_users, koji_added_users, custom_build_permissions_added, custom_build_permissions_removed)
    # Adding custom build permissions
    if len(custom_build_add) > 0:
        print(f"\nACTION: Going to add build- permissions for {len(custom_build_add)} user.")
        for user,permissions in custom_build_add.items():
            custom_build_status = change_koji_perms(user, "grant", permissions, koji_admin_session, True)
            if custom_build_status[0]:
                custom_build_permissions_added[user] = permissions
            else:
                subject = "Koji synchronize users - Koji error"
                body = f"ERROR: An error happened while adding a custom build permission.\nError report:\n{custom_build_permissions_added[1]}"
                send_email(ADMIN_EMAIL, subject, body)
                print(body)
                return (activated_users, deactivated_users, koji_added_users, custom_build_permissions_added, custom_build_permissions_removed)
    # Removing custom build permissions
    if len(custom_build_remove) > 0:
        print(f"\nACTION: Going to remove build- permissions for {len(custom_build_add)} user.")
        for user,permissions in custom_build_remove.items():
            custom_build_status = change_koji_perms(user, "revoke", permissions, koji_admin_session, True)
            if custom_build_status[0]:
                custom_build_permissions_removed[user] = permissions
            else:
                subject = "Koji synchronize users - Koji error"
                body = f"ERROR: An error happened while removing a custom build permission.\nError report:\n{custom_build_permissions_added[1]}"
                send_email(ADMIN_EMAIL, subject, body)
                print(body)
                return (activated_users, deactivated_users, koji_added_users, custom_build_permissions_added, custom_build_permissions_removed)

    return activated_users, deactivated_users, koji_added_users, custom_build_permissions_added, custom_build_permissions_removed


"""
    actions_koji_admins() manages all actions that need to take place in koji.
    These actions are: activate a koji user, deactivate a koji user and add a new CERN user in koji.
    ARG:   a set of users to activate, a set of users to deactivate, a set of users to add and
            a koji.ClientSession with a logged in user with koji admin rights
    RET:    3 lists of dictionaries: activated koji users, deactivated koji users and newly added koji users.
"""
def actions_koji_admins(koji_users_to_admin, koji_users_revoke_admin, eadmins_not_koji, koji_admin_session):

    granted_admin_perm = []
    revoked_admin_perm = []
    koji_added_admins = []

    # Giving admin perm to a user
    if len(koji_users_to_admin) > 0:
        print(f"\nACTION: Going to grant admin permission to {len(koji_users_to_admin)} user(s) in koji.")
        for kuta in koji_users_to_admin:
            upgraded_user_perms = change_koji_perms(kuta, "grant", ["admin"], koji_admin_session, False)
            if upgraded_user_perms[0]:
                granted_admin_perm.append(upgraded_user_perms[1])
            else:
                subject = "Koji synchronize users - Koji error"
                body = f"ERROR: An error happened while granting admin permission to a koji user.\n\nError report:\n{upgraded_user_perms[1]}"
                send_email(ADMIN_EMAIL, subject, body)
                print(body)
                return granted_admin_perm, revoked_admin_perm, koji_added_admins

    # Revoke admin perm from an admin
    if len(koji_users_revoke_admin) > 0:
        print(f"\nACTION: Going to revoke admin permission to {len(koji_users_revoke_admin)} user(s) in koji.")
        for kura in koji_users_revoke_admin:
            downgraded_user_perms = change_koji_perms(kura, "revoke", ["admin"], koji_admin_session, False)
            if downgraded_user_perms[0]:
                revoked_admin_perm.append(downgraded_user_perms[1])
            else:
                subject = "Koji synchronize users - Koji error"
                body = f"ERROR: An error happened while revoking admin permission from a koji user.\n\nError report:\n{downgraded_user_perms[1]}"
                send_email(ADMIN_EMAIL, subject, body)
                print(body)
                return granted_admin_perm, revoked_admin_perm, koji_added_admins

    # Adding admins
    if len(eadmins_not_koji) > 0:
        print(f"\nACTION: Going to add {len(eadmins_not_koji)} egroup_admins in koji with admin rights.")
        for enk in eadmins_not_koji:
            koji_added_admin_status = add_to_koji(enk, ["admin"], koji_admin_session)
            if koji_added_admin_status[0]:
                koji_added_admins.append(koji_added_admin_status[1])
            else:
                subject = "Koji synchronize users - Koji error"
                body = f"ERROR: An error happened while adding a new admin in koji.\nError report:\n{koji_added_admin_status[1]}"
                send_email(ADMIN_EMAIL, subject, body)
                print(body)
                return granted_admin_perm, revoked_admin_perm, koji_added_admins

    return granted_admin_perm, revoked_admin_perm, koji_added_admins


"""
    add_to_koji() adds a user in koji.
    ARG:    A CERN user, a koji.ClientSession with a logged in user with koji admin rights
    RET:    tuple = (Bool: True for success, False for fail,
					Dictionary: representing the new koji user for success, String: error for fail)
"""
def add_to_koji(user_not_koji, koji_perms, koji_admin_session):

    print(f"'{user_not_koji}' will be added in koji as active user with {koji_perms} rights.")
    krb_principals = user_not_koji + "@CERN.CH"
    try:
        new_user_id = koji_admin_session.createUser(user_not_koji, 0, krb_principals)
    except koji.GenericError:
        error = f"koji.GenericError: Unable to add this new user to koji: '{user_not_koji}'. This user already exists."
        return (False, error)
    print(f"The user '{user_not_koji}' that was just created has ID = {new_user_id}")
    try:
        error_type = "retrieve"
        new_user = koji_admin_session.getUser(user_not_koji, strict=True)
        error_type = "give permissions to"
        for perm in koji_perms:
            koji_admin_session.grantPermission(new_user["name"], perm)
        error_type = "retrieve permissions of"
        new_user_perms = koji_admin_session.getUserPerms(new_user["name"])
        new_user["perms"] = new_user_perms
    except koji.GenericError:
        error = f"koji.GenericError: Cannot {error_type} the newly added koji user: '{user_not_koji}'"
        return (False, error)
    print(f"The newly created user is: {new_user} with permissions set to: {new_user_perms}")

    return (True, new_user)


"""
    change_koji_status() changes the status of a koji_user.
    ARG:    A koji user (username), a koji.ClientSession with a logged in user with koji admin rights
    RET:    tuple = (Bool: True for success, False for fail,
					Dictionary: representing the koji user with updated status for success, String: error for fail)

"""
def change_koji_status(koji_user_name, status, koji_admin_session):

    print(f"Current koji user is:\t{koji_user_name} and the status will change to {status}")
    if status not in [0, 1]:
        error = f"'{status}' : is an invalid koji status while working on user '{koji_user_name}' The valid koji status values are 0 or 1."
        return (False, error)
    try:
        if status == 0:
            koji_admin_session.enableUser(koji_user_name)
        elif status == 1:
            koji_admin_session.disableUser(koji_user_name)
    except koji.GenericError:
        error = f"koji.GenericError: Unable to change status to koji user:\n{koji_user_name}\t\t{koji_user_name}"
        return (False, error)
    try:
        updated_user = koji_admin_session.getUser(koji_user_name, strict=True)
    except koji.GenericError:
        error = f"koji.GenericError: Could not retrieve the recently upadated koji user '{koji_user_name}' from koji database."
        return (False, error)
    print(f"Updated user is:\t{updated_user}")

    return (True, updated_user)


"""
    change_koji_perms() changes the permissions of a koji_user.
    ARG:    A koji user (username), a koji.ClientSession with a logged in user with koji admin rights
    RET:    tuple = (Bool: True for success, False for fail,
					Dictionary: representing the koji user with updated status for success, String: error for fail)

"""
def change_koji_perms(koji_user_name, change_type, koji_perms, koji_admin_session, createnew):
    try:
        current_koji_perms = koji_admin_session.getUserPerms(koji_user_name)
        koji_user = koji_admin_session.getUser(koji_user_name, strict=True)
        print(f"'{koji_user_name}' current permissions are: {current_koji_perms}. Permision(s) {koji_perms} will be {change_type}-ed.")
    except koji.GenericError:
        error = f"koji.GenericError: Could not retrieve koji user '{koji_user_name}' from koji database. User does not exist."
        return (False, error)

    if change_type not in ["grant", "revoke"]:
        error = f"'{change_type}' : is an invalid change in permisions for user '{koji_user_name}'. Use 'grant' or 'revoke'."
        return (False, error)

    updated_permission = False
    if change_type == "grant":
        for perm in koji_perms:
            if perm not in current_koji_perms:
                try:
                    koji_admin_session.grantPermission(koji_user_name, perm, create=createnew)
                    print(f"Added {perm} permission for user {koji_user_name}")
                    updated_permission = True
                except koji.GenericError:
                    error = f"koji.GenericError: Unable to grant permission '{perm}' to koji user:\t{koji_user_name}."
                    error += f"\n{koji_user_name}'s current permissions are: {current_koji_perms}"
                    return (False, error)

    if change_type == "revoke":
        for perm in koji_perms:
            try:
                koji_admin_session.revokePermission(koji_user_name, perm)
            except koji.GenericError:
                error = f"koji.GenericError: Unable to revoke permission '{perm}' from koji user:\t{koji_user_name}."
                error += f"\n{koji_user_name}'s current permissions are: {current_koji_perms}"
                return (False, error)

    if updated_permission:
        # Refresh
        current_koji_perms = koji_admin_session.getUserPerms(koji_user_name)
        print(f"'{koji_user_name}' new koji permissions are:\t{current_koji_perms}")
    return (True, koji_user)


"""
    success_email_users() sends email to ADMIN_EMAIL to inform for which koji users their status changed succesfully
    and which CERN users were added succesfully in koji.
	ARGS:	3 lists of dictionaries: activated koji users, deactivated koji users and newly added koji users.
"""
def success_email_users(activated_users, deactivated_users, koji_added_users, custom_build_add, custom_build_remove):

    subject = f"New changes on Koji users."
    body = "Dear Koji admins,\n\n"
    body += "The following changes on koji took place recently:\n"
    if len(activated_users) > 0:
        for au in activated_users:
            body += f"\n{au['name']} is now activated: {au}"
    if len(deactivated_users) > 0:
        for du in deactivated_users:
            body += f"\n{du['name']} is now deactivated: {du}"
    if len(koji_added_users) > 0:
        for kau in koji_added_users:
            body += f"\n{kau['name']} is now added in koji: {kau}"
    if len(custom_build_add) > 0:
        for k,v in custom_build_add.items():
            body += f"\nThe following permissions were added for user {k}: {v}"
    if len(custom_build_remove) > 0:
        for k,v in custom_build_remove.items():
            body += f"\nThe following permissions were removed for user {k}: {v}"

    body += "\n\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)"

    send_email(ADMIN_EMAIL, subject, body)


"""
    success_email_admins() sends email to ADMIN_EMAIL to inform for which koji users their status changed succesfully
    and which CERN users were added succesfully in koji.
	ARGS:	3 lists of dictionaries: activated koji users, deactivated koji users and newly added koji users.
"""
def success_email_admins(upgraded_users, downgraded_users, koji_added_admins):

    subject = f"New changes on Koji users."
    body = "Dear Koji admins,\n\n"
    body += "The following changes on koji users and admins took place recently:\n"
    if len(upgraded_users) > 0:
        for au in upgraded_users:
            body += f"\n{au['name']} is now granted with admin permission: {au}"
    if len(downgraded_users) > 0:
        for du in downgraded_users:
            body += f"\n{du['name']} has admin permission revoked: {du}"
    if len(koji_added_admins) > 0:
        for kau in koji_added_admins:
            body += f"\n{kau['name']} is now added in koji with admin permissions: {kau}"

    body += "\n\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)"

    send_email(ADMIN_EMAIL, subject, body)


"""
	send_email() works as function-template for sending email(s)
	ARGS:	receiver of the email, the subject of the email, the body of the email,	email sender
"""
def send_email(email_to, subject, body, email_from='linux.support@cern.ch'):

	server = smtplib.SMTP('cernmx.cern.ch')
	msg = MIMEMultipart()
	msg['Subject'] = subject
	msg['From'] = email_from
	msg['To'] = email_to
	msg.add_header('reply-to', 'noreply.Linux.Support@cern.ch')
	body = MIMEText(f"{body}", _subtype='plain')
	msg.attach(body)

	try:
		server.sendmail(email_from, email_to, msg.as_string())
		time.sleep(2)
	except:
		print(f"failed to send email to {email_to}, continuing...")



if __name__ == "__main__":

    main()
